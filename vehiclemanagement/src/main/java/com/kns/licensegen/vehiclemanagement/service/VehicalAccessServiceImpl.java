package com.kns.licensegen.vehiclemanagement.service;

import com.kns.licensegen.vehiclemanagement.dto.Vehical;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;


@Service
public class VehicalAccessServiceImpl implements VehicalAccessService{

    @Override
    public Vehical findVehical(String id){
        Vehical sampleVehical = new Vehical();
        sampleVehical.setId("KU-9377");
        sampleVehical.setChasisNo("abcd");
        sampleVehical.setEngineNo("pqrs");
        sampleVehical.setOwner("lmno");
        Date date = Calendar.getInstance().getTime();
        sampleVehical.setRegistrationDate(date);
        return  sampleVehical;
    }
}
