package com.kns.licensegen.vehiclemanagement.dto;

import lombok.Data;
import java.util.Date;

@Data
public class Vehical {

    String id;
    String engineNo;
    String chasisNo;
    String owner;
    Date registrationDate;
}
