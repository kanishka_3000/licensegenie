package com.kns.licensegen.vehiclemanagement.service;

import com.kns.licensegen.vehiclemanagement.dto.Vehical;

public interface VehicalAccessService {

    Vehical findVehical(String id);
}
