package com.kns.licensegen.vehiclemanagement.config;

import com.kns.licensegen.vehiclemanagement.service.VehicalAccessService;
import com.kns.licensegen.vehiclemanagement.service.VehicalAccessServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    VehicalAccessService vehicalAccessService(){
        return new VehicalAccessServiceImpl();
    }
}
