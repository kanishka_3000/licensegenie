package com.kns.licensegen.vehiclemanagement.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kns.licensegen.vehiclemanagement.dto.Vehical;
import com.kns.licensegen.vehiclemanagement.service.VehicalAccessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;

@RestController
@RequestMapping("api/vehical")
@Slf4j
public class InformationQuery {

    Gson gson = new GsonBuilder().create();

    @Autowired
    VehicalAccessService vehicalAccessService;

    @GetMapping("find")
    @ResponseBody String findVehicle(){
        log.trace("A request was received");

        Vehical vehical = vehicalAccessService.findVehical("");

        String response = gson.toJson(vehical);
        log.trace(response);
        return response;
    }

}
