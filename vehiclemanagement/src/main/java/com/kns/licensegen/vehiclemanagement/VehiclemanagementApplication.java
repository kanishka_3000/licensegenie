package com.kns.licensegen.vehiclemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;

@SpringBootApplication
@ComponentScan(basePackages ="com.kns")
public class VehiclemanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehiclemanagementApplication.class, args);
	}

}
