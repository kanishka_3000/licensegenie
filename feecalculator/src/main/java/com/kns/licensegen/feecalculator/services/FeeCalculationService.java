package com.kns.licensegen.feecalculator.services;

import com.kns.licensegen.feecalculator.dto.Fee;

public interface FeeCalculationService {
    Fee calculateGeneral(int vehicalClass);

}
