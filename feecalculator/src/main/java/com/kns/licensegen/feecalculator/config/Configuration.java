package com.kns.licensegen.feecalculator.config;

import com.kns.licensegen.feecalculator.services.FeeCalculationService;
import com.kns.licensegen.feecalculator.services.FeeCalculationServiceImpl;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    FeeCalculationService feeCalculationService(){
        return new FeeCalculationServiceImpl();
    }
}
