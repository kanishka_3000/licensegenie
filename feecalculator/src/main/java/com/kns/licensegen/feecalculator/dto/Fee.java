package com.kns.licensegen.feecalculator.dto;

import lombok.Data;

@Data
public class Fee {
    Double basic;
    Double tax;
    Double lateFee;
}
