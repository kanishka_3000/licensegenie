package com.kns.licensegen.feecalculator.services;

import com.kns.licensegen.feecalculator.dto.Fee;

public class FeeCalculationServiceImpl implements FeeCalculationService{

    @Override
    public Fee calculateGeneral(int vehicalClass) {
        Fee fee = new Fee();
        fee.setBasic(300.0);
        fee.setTax(40.0);
        fee.setLateFee(0.0);
        return fee;
    }
}
