package com.kns.licensegen.feecalculator.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kns.licensegen.feecalculator.dto.Fee;
import com.kns.licensegen.feecalculator.services.FeeCalculationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/fee")
public class FeeCalculate {

    Gson gson = new GsonBuilder().create();

    @Autowired
    FeeCalculationService feeCalculationService;

    @GetMapping("general")
    public @ResponseBody String calculateGeneralFee(){
        log.trace("Request Received");
        Fee fee = feeCalculationService.calculateGeneral(-1);

        String output = gson.toJson(fee);
        log.trace(output);
        return output;
    }
}
