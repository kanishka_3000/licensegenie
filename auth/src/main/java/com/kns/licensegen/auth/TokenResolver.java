package com.kns.licensegen.auth;

public interface TokenResolver {

    void setSecret(String secret);
    Token getToken(String token);
}
