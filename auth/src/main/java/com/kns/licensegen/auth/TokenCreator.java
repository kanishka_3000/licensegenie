package com.kns.licensegen.auth;

import java.util.Map;

public interface TokenCreator {
    void setExpiration(int expiration);
    void setSecret(String secret);
    String generate(String username, Map<String, Object> claims);
}
