package com.kns.licensegen.auth;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.Map;

public class TokenCreatorImpl implements TokenCreator{
    private String secret = Config.secret;
    private int expiration = 10;

    @Override
    public void setExpiration(int expiration) {
        this.expiration = expiration;

    }

    @Override
    public void setSecret(String secret) {
        this.secret = secret;

    }

    @Override
    public String generate(String username, Map<String, Object> claims) {
        String token = Jwts.builder().setClaims(claims).setSubject(username).setIssuedAt(new Date(System.currentTimeMillis())).
                setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * expiration)).
                signWith(SignatureAlgorithm.HS512, secret).compact();
        return token;
    }
}
