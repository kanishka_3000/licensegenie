package com.kns.licensegen.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.util.Date;
import java.util.function.Function;

public class TokenResolverImpl implements TokenResolver{

    private String secret = Config.secret;

    @Override
    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public Token getToken(String token) {
        Token token1 = new Token();
        Date expirationDate = extractClaim(token, Claims::getExpiration);
        token1.setExpirationDate(expirationDate);

        String username = extractClaim(token, Claims::getSubject);
        token1.setUsername(username);
        return token1;
    }

    private Claims extractAllClaims(String token){
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    public  <T> T extractClaim(String token, Function<Claims, T> claimsResolver){
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
}
