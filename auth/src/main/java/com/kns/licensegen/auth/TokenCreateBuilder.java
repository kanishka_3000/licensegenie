package com.kns.licensegen.auth;

public class TokenCreateBuilder {
    TokenCreator tokenCreator;
    private TokenCreateBuilder(){}

    public TokenCreateBuilder builder(){
        TokenCreateBuilder tokenCreateBuilder= new TokenCreateBuilder();
        TokenCreator tokenCreator = new TokenCreatorImpl();
        tokenCreateBuilder.tokenCreator = tokenCreator;
        return tokenCreateBuilder;
    }

    public TokenCreateBuilder setExpiration(int expiration){
        this.tokenCreator.setExpiration(expiration);
        return this;
    }

    public TokenCreateBuilder setSecret(String secret){
        this.tokenCreator.setSecret(secret);
        return this;
    }

    public TokenCreator get(){
        return this.tokenCreator;
    }
}
