package com.kns.licensegen.auth;

public class TokenResolverBuilder {

    TokenResolver tokenResolver;
    private TokenResolverBuilder(){

    }
    public TokenResolverBuilder builder(){
        TokenResolverBuilder tokenResolverBuilder = new TokenResolverBuilder();
        this.tokenResolver = new TokenResolverImpl();
        return  tokenResolverBuilder;
    }

    public TokenResolverBuilder setSecret(String secret){
        this.tokenResolver.setSecret(secret);
        return this;
    }
    public TokenResolver get(){
        return this.tokenResolver;
    }
}
